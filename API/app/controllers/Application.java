package controllers;

import play.mvc.Controller;
import play.mvc.Result;
import views.html.hello;
import views.html.index;
import views.html.multiply;
import views.html.sum;

public class Application extends Controller {

    public static Result index() {
        return ok(index.render("nbbbb."));
    }
    
    public static Result hello(String name) {
    	if (name == null) {
    		return badRequest("Błąd = ");
    	}
    	
    	return ok(hello.render(name));
    }
    
    public static Result sum(int x, int y) {
    int 	rr = x+y;
    	return ok(sum.render(rr));
   
//    	return ok(String.format("a=%d, b=%d, a+b=%d",	 x, y, y+x));
    }
    public static Result multiply(int a, int b) {
       
        	return ok(multiply.render(a, b, a*b));
       
//        	return ok(String.format("a=%d, b=%d, a+b=%d",	 x, y, y+x));
        }
 
}