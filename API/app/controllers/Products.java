package controllers;

import com.avaje.ebean.Page;
import models.Product;

import play.data.Form;
import play.mvc.Controller;
import play.mvc.Result;
import views.html.*;

import java.util.List;

public class Products extends Controller {

	private static final Form<Product> productForm = Form.form(Product.class);

	public static Result list(Integer page) {
		Page<Product> products = Product.find(page);
		return ok(views.html.list.render(products));
	}

	public static Result newProduct() {
		return ok(views.html.productDetails.render(productForm));
	}

	public static Result details(Product product) {
		if (null == product) {
			return notFound(String.format("Product does not exist"));
		}
		Form<Product> filledForm = productForm.fill(product);
		return ok(productDetails.render(filledForm));
	}

	public static Result save() {
		Form<Product> productFromRequest = Products.productForm.bindFromRequest();
		if (productFromRequest.hasErrors()) {
			flash("error", "Please correct the form below.");
			return badRequest(views.html.productDetails.render(productFromRequest));
		}

		Product product = productFromRequest.get();

		System.out.println(product);
		System.out.println(product.id);

		if (product.id == null) {
			product.save();
		} else {
			product.update();
		}

		flash("success", String.format("Successfully added product %s", product));
		return redirect(routes.Products.list(0));
	}

	public static Result delete(String ean) {
		Product product = Product.findByEan(ean);
		if (null == product) {
			return notFound(String.format("Product %s does not exist", ean));
		}

		product.delete();
		return redirect(routes.Products.list(0));
	}
}