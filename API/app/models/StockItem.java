package models;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.ManyToOne;

import play.db.ebean.Model;

@Entity
public class StockItem extends Model {

	@Id
	public long id;

	@ManyToOne
	public Warehouse warehouse;

	@ManyToOne
	public Product product;

	public Long quantity;

	@Override
	public String toString() {
		return "StockItem [id=" + id + ", warehouse=" + warehouse + ", product=" + product + ", quantity=" + quantity
				+ "]";
	}

}
