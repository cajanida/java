package models;

import java.util.List;
import java.util.Map;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.OneToMany;

import com.avaje.ebean.Page;

import play.data.validation.Constraints.MinLength;
import play.data.validation.Constraints.Required;
import play.db.ebean.Model;
import play.libs.F;
import play.mvc.PathBindable;
import play.mvc.QueryStringBindable;

@Entity
public class Product extends Model implements PathBindable<Product>, QueryStringBindable<Product> {

	@Id
	public Long id;
	@Required
	public String ean;
	@Required
	public String name;
	@Required
	@MinLength(5)
	public String description;
	@OneToMany(mappedBy = "product")
	public List<StockItem> stockItem;

	public Product(String ean, String name, String description) {
		this.ean = ean;
		this.name = name;
		this.description = description;
	}

	@Override
	public String toString() {
		// return "Product [ean=" + ean + ", name=" + name + ", description=" +
		// description + "]";
		return String.format("%s - %s", ean, name);
	}

	// -----------------BINDER-------------------
	@Override
	public Product bind(String key, String value) {
		return findByEan(value);
	}

	@Override
	public String javascriptUnbind() {
		return this.ean;
	}

	@Override
	public String unbind(String arg0) {
		return this.ean;
	}

	// /product/?ean
	@Override
	public F.Option<Product> bind(String key, Map<String, String[]> data) {
		return F.Option.Some(findByEan(data.get(key)[0]));
	}
	// JSR 303
	// ----------------------------DAO------------------------------

	public static Finder<Long, Product> find = new Finder<>(Long.class, Product.class);

	public static Page<Product> find(Integer page) {
		return find.where()
				.orderBy("id asc")
				.findPagingList(10)
				.setFetchAhead(false)
				.getPage(page);
	}

	public static Product findByEan(String ean) {

		return find.where().eq("ean", ean).findUnique();
	}

}
