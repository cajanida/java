package models;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;

import com.avaje.ebean.Page;

import play.db.ebean.Model;
import play.db.ebean.Model.Finder;
import play.libs.F.Option;
import play.mvc.PathBindable;
import play.mvc.QueryStringBindable;

@Entity
public  class Warehouse extends Model implements PathBindable<Warehouse>, QueryStringBindable<Warehouse>  {

	@Id
	public Long id;
	public String name;
	public String city;
	public String postalCode;
	@OneToMany(mappedBy = "warehouse")
	public List<StockItem> stock = new ArrayList<>();

	public Warehouse(Long id, String name, String city, String postalCode){
		this.id= id;
		this.name = name;
		this.city = city;
		this.postalCode = postalCode;
	}
	
	public String toString(){
		return String.format("%s - %s", id, name);
	}

	@Override
	public Option<Warehouse> bind(String arg0, Map<String, String[]> arg1) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public Warehouse bind(String arg0, String arg1) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public String javascriptUnbind() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public String unbind(String arg0) {
		// TODO Auto-generated method stub
		return null;
	}
	public static Finder<Long, Warehouse> find = new Finder<>(Long.class, Warehouse.class);

	public static Page<Warehouse> find(Integer page) {
		return find.where()
				.orderBy("id asc")
				.findPagingList(10)
				.setFetchAhead(false)
				.getPage(page);
	}


}
