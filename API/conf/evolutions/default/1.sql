# --- Created by Ebean DDL
# To stop Ebean DDL generation, remove this comment and start using Evolutions

# --- !Ups

create table product (
  id                        bigint auto_increment not null,
  ean                       varchar(255),
  name                      varchar(255),
  description               varchar(255),
  constraint pk_product primary key (id))
;

create table stock_item (
  id                        bigint auto_increment not null,
  warehouse_id              bigint,
  product_id                bigint,
  quantity                  bigint,
  constraint pk_stock_item primary key (id))
;

create table warehouse (
  id                        bigint auto_increment not null,
  name                      varchar(255),
  city                      varchar(255),
  postal_code               varchar(255),
  constraint pk_warehouse primary key (id))
;

alter table stock_item add constraint fk_stock_item_warehouse_1 foreign key (warehouse_id) references warehouse (id) on delete restrict on update restrict;
create index ix_stock_item_warehouse_1 on stock_item (warehouse_id);
alter table stock_item add constraint fk_stock_item_product_2 foreign key (product_id) references product (id) on delete restrict on update restrict;
create index ix_stock_item_product_2 on stock_item (product_id);



# --- !Downs

SET FOREIGN_KEY_CHECKS=0;

drop table product;

drop table stock_item;

drop table warehouse;

SET FOREIGN_KEY_CHECKS=1;

