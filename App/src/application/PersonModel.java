package application;

import javafx.beans.property.*;

public class PersonModel {
	private SimpleStringProperty name;
	private SimpleStringProperty surname;
	private SimpleLongProperty id;
	private SimpleLongProperty date;

	public PersonModel(String name, String surname, int id, int date) {
		this.name = new SimpleStringProperty(name);
		this.surname = new SimpleStringProperty(surname);
		this.id = new SimpleLongProperty(id);
		this.date = new SimpleLongProperty(date);
	}

	public SimpleStringProperty getName() {
		return name;
	}

	public void setName(SimpleStringProperty name) {
		this.name = name;
	}

	public SimpleStringProperty getSurname() {
		return surname;
	}

	public void setSurname(SimpleStringProperty surname) {
		this.surname = surname;
	}

	public SimpleLongProperty getId() {
		return id;
	}

	public void setId(SimpleLongProperty id) {
		this.id = id;
	}

	public SimpleLongProperty getDate() {
		return date;
	}

	public void setDate(SimpleLongProperty date) {
		this.date = date;
	}


}
