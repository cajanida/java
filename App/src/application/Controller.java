package application;

import java.net.URL;
import java.util.ResourceBundle;

import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Button;
import javafx.scene.control.DatePicker;
import javafx.scene.control.Label;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.TextField;

public class Controller implements Initializable {

	public ObservableList<PersonModel> list;

	@FXML
	private Label name;
	@FXML
	private Label surname;
	@FXML
	private Label ID;
	@FXML
	private Label birthDate;
	@FXML
	private TextField nameText;
	@FXML
	private TextField surnameText;
	@FXML
	private TextField IDText;
	@FXML
	private DatePicker birthDatePicker;
	@FXML
	private Button save;
	@FXML
	private Button clear;
	@FXML
	private TableView<PersonModel> table;
	@FXML
	private TableColumn<PersonModel, String> userName;
	@FXML
	private TableColumn<PersonModel, String> userSurname;

	public PersonListController prc = new PersonListController();

	@FXML
	private void clearTextFields(ActionEvent event) {
		this.nameText.clear();
		this.surnameText.clear();
		this.IDText.clear();
		this.birthDatePicker.setValue(null);
		this.list.add(new PersonModel("imie3", "nazwisko3", 123, 456));
	}



	@Override
	public void initialize(URL arg0, ResourceBundle arg1) {

		table.getSelectionModel().selectedIndexProperty().addListener(new ChangeListener<Number>() {
		     @Override
		     public void changed(ObservableValue<? extends Number> paramObservableValue,Number prevRowIndex, Number currentRowIndex) {
		          System.out.println(currentRowIndex);
		          PersonModel x = list.get((int) currentRowIndex);
		          System.out.println(x.getName().getValue());
		     }
		});

		this.userName.setCellValueFactory(cellData->cellData.getValue().getName());
		this.userSurname.setCellValueFactory(cellData->cellData.getValue().getSurname());

		this.list = FXCollections.observableArrayList();
		this.list.add(new PersonModel("imie0", "nazwisko0", 123, 456));
		this.list.add(new PersonModel("imie1", "nazwisko2", 123, 456));

		this.table.setItems(this.list);
	}

}