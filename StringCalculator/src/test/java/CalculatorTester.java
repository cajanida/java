import static org.junit.Assert.*;
import org.junit.Test;



public class CalculatorTester {
	
	
	private Calculator sut = new Calculator();
	@Test
	public void shouldReturnZeroIfEmptyStringGiven() throws Exception{
		//when
		int actual = sut.add("");
		
		
		//then
		assertEquals(0, actual);
		
	}
	@Test
	public void shouldReturnOneIfOneIsPassed() throws Exception{
		//when
		int actual = sut.add("1");
		
		
		//then
		assertEquals(1, actual);
		
	}
}
