 import java.awt.*;
import java.awt.event.*;

public class app1 implements ActionListener {

    // definicja p�l obiektu
    public TextField t;
    public Button b, b2, b3;
    public Label lb; 
    public Frame fr; 
    app1() {
        // tworzymy ramke
        fr = new Frame();
        // initializujemy etykiete
        lb = new Label("ID: ");
        lb.setBounds(1, 1, 50, 50);
        // initializujemy pole tekstowe
        t = new TextField();
        t.setBounds(200, 200, 50, 50);
        // oraz przyciski
        b = new Button("Uzupe�nij");
        b2 = new Button("Zamknij");
        b3 = new Button("Uzupe�nij label");
        // ustawiamy wymiary przyciskow i ich koordynanty
        b.setBounds(50, 50, 50, 50);
        b2.setBounds(150, 150, 50, 50);
        b3.setBounds(250, 50, 150, 50);
        
        // dodajemy nasluchwianie zdarzen dla przyciskow
        b.addActionListener(this);
        b2.addActionListener(this);
        b3.addActionListener(new ClickActionListener(lb));
        
        fr.addWindowListener( new WindowAdapter() {
            @Override
            public void windowClosing(WindowEvent we) {
                System.out.println("Zamykamy :(");
                System.exit(0);
            }
        } );
        
        
        // dodajemy elementy do ramki
        fr.add(lb);
        fr.add(t);
        fr.add(b);
        fr.add(b2);
        fr.add(b3);
        // ustawiamy wielkosc ramki
        fr.setSize(500, 300);

        // definiujemy typ layoutu ramki
        fr.setLayout(null);
        // ustawiamy widocznosc ramki
        fr.setVisible(true);
    }
    @Override
    public void actionPerformed(ActionEvent event) {
        if (event.getSource() == this.b) {
            System.out.println("xx");
            String x = b.getLabel();
            this.t.setText(x);
        } else {
            System.out.println(event.getSource());
            System.exit(0);
        }
    }
    public static void main(String args[]) {
        new app1();
    }
}